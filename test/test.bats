#!/usr/bin/env bats

setup() {
  DOCKER_IMAGE=${DOCKER_IMAGE:="test/demo-pipe-bash"}

  echo "Building image..."
  docker build -t ${DOCKER_IMAGE}:test .
}

@test "Dummy test" {

	status=0
    echo "Status: $status"
    echo "Output: $output"

    [ "$status" -eq 0 ]
}

