# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 1.0.1

- patch: Fix last information

## 1.0.0

- major: releasing major version

## 0.2.0

- minor: updated documentation

## 0.1.0

- minor: added timeout and retries capabilities in 0.1.6

## 0.0.3

- patch: added dynamic variable

## 0.0.2

- patch: add variable
- patch: add variable 2

## 0.0.1

- patch: test variables

