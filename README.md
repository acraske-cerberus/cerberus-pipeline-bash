# Cerberus Testing Pipelines Pipe: CI/CD Integration

This pipe is used to integrate Cerberus Testing within your development process for the testing phase.

The pipeline template is to be defined for each stage of your CI/CD process with the defined parameters.



## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
- pipe: docker://ataleio/cerberus-pipeline-bash:1.0.1
   environment:
     CERB_ENV_VAR_HOST: $CERB_ENV_VAR_HOST
     CERB_ENV_VAR_CAMPAIGN: $CERB_ENV_VAR_CAMPAIGN
```
## Variables

Variables and their corresponding usage :

- CERB_ENV_VAR_HOST (*)     : Name of the Cerberus instance to use, including http/https         
- CERB_ENV_VAR_CAMPAIGN (*) : The name of the campaign to trigger                               
- CERB_ENV_VAR_TIMEOUT      : The maximum time in seconds to wait for campaign end (in seconds) 
- CERB_ENV_VAR_CHECK        : The interval in seconds to check for end of the campaign (in seconds) 

_(*) = required variable._

## Prerequisites
- A Cerberus instance available through the mentioned URL
- A defined Campaign to execute, that include the environment, countries, browsers to execute to

## Examples

Basic example:

```yaml
script:
  - pipe: docker://ataleio/cerberus-pipeline-bash:1.0.1
            environment:
             CERB_ENV_VAR_HOST: https://myinstance.cerberus-testing.com
             CERB_ENV_VAR_CAMPAIGN: my-test-campaign-integration
```

Advanced example, with variables defined in your repository or injected to the docker image, with 2 testing steps:

```yaml
pipelines:
  default:
  - step:
      name: Cerberus Testing Phase - QA
      script:
          - pipe: docker://ataleio/cerberus-pipeline-bash:1.0.1
            environment:
             CERB_ENV_VAR_HOST: $CERB_ENV_VAR_HOST
             CERB_ENV_VAR_CAMPAIGN: $CERB_ENV_VAR_CAMPAIGN (QA Campaign)
             CERB_ENV_VAR_TIMEOUT: 100 
  
  - step:
  	# Deploy to QA, other phases
  
  - step:
      name: Cerberus Testing Phase - UAT
      script:
          - pipe: docker://ataleio/cerberus-pipeline-bash:1.0.1
            environment:
             CERB_ENV_VAR_HOST: $CERB_ENV_VAR_HOST
             CERB_ENV_VAR_CAMPAIGN: $CERB_ENV_VAR_CAMPAIGN (UAT Campaign)
             CERB_ENV_VAR_TIMEOUT: 600
             CERB_ENV_VAR_CHECK: 30
```

## Support
If you’d like help with this pipe, or you have an issue or feature request, let us know.
The pipe is maintained by contact@cerberus-testing.com.

If you’re reporting an issue, please include:

- the version of the pipe
- relevant logs and error messages
- steps to reproduce
